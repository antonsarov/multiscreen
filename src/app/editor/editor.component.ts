import {ChangeDetectorRef, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BcService} from '../bc.service';
import {ActivatedRoute} from '@angular/router';

export interface Message {
  foo: string;
  d: number;
}

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {

  data: Message;
  standalone = false;
  @Output() close = new EventEmitter();

  constructor(private bc: BcService, private route: ActivatedRoute, private cdRef: ChangeDetectorRef) {
    console.log('c');
  }

  ngOnInit() {
    console.log('init');
    const routeData = this.route.snapshot.data.a;
    if (routeData !== undefined) {
      this.standalone = routeData;
      this.bc.source$.subscribe((msg: Message) => {
        console.log(msg);
        this.data = msg;
        this.cdRef.detectChanges();
      });
    } else {
      this.bc.source2$.subscribe((msg: Message) => {
        console.log(msg);
        this.data = msg;
        this.cdRef.detectChanges();
      });
    }

  }

  detach() {
    console.log('detached');
    this.close.emit(null);
  }
}
