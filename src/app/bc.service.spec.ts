import { TestBed } from '@angular/core/testing';

import { BcService } from './bc.service';

describe('BcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BcService = TestBed.get(BcService);
    expect(service).toBeTruthy();
  });
});
