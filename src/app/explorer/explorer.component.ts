import { Component, OnInit } from '@angular/core';
import {Message} from '../editor/editor.component';
import {BcService} from '../bc.service';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.css']
})
export class ExplorerComponent implements OnInit {

  constructor(private bc: BcService) {
    console.log('c');
  }

  ngOnInit() {
  }

  send(name) {
    console.log('sending');
    this.bc.send({
      foo: name,
      d: Date.now()
    });
  }

}
