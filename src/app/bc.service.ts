import { Injectable } from '@angular/core';
import BroadcastChannel from 'broadcast-channel';
import {BehaviorSubject, Subject} from 'rxjs';
import {Message} from './editor/editor.component';

@Injectable({
  providedIn: 'root'
})
export class BcService {

  channel = new BroadcastChannel('foobar');
  private source = new BehaviorSubject<Message>({foo: '', d: Date.now()});
  source$ = this.source.asObservable();

  private source2 = new Subject<Message>();
  source2$ = this.source2.asObservable();

  constructor() {
    console.log('c');
    this.channel.onmessage = msg => {
      console.log('onm');
      this.source.next(msg);
    };
  }

  send(message) {
    this.source2.next(message);
    this.channel.postMessage(message);
  }
}
